package cn.atuyang.work;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

import java.io.IOException;

/**
 * @author aTuYang
 */
public class GeekTextInputFormat extends TextInputFormat {

    @Override
    public RecordReader<LongWritable, Text> getRecordReader(InputSplit genericSplit, JobConf job, Reporter reporter) throws IOException {
        return super.getRecordReader(genericSplit, job, reporter);
    }

    public static class GeekRecordReader implements RecordReader<LongWritable, Text> {

        @Override
        public boolean next(LongWritable longWritable, Text text) throws IOException {
            return false;
        }

        @Override
        public LongWritable createKey() {
            return null;
        }

        @Override
        public Text createValue() {
            return null;
        }

        @Override
        public long getPos() throws IOException {
            return 0;
        }

        @Override
        public void close() throws IOException {

        }

        @Override
        public float getProgress() throws IOException {
            return 0;
        }
    }
}
